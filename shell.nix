# commit from unstable from 2021-05-21
{ pkgs ? import (fetchTarball "https://github.com/NixOS/nixpkgs/archive/22c86a0466969819fb7bf9c7a0c65711e5f1a005.tar.gz") {}}:
with pkgs;
let
  inherit (lib) optionals;
  erlang24 = erlangR23.override {
    version = "24.0.1";
    # nix-prefetch-url --unpack https://github.com/erlang/otp/archive/OTP-24.0.1.tar.gz
    sha256 = "1j2y3m1k75c62gvhvj5f2jw3sijii1z8bjnk547w4ak4ldl60kfg";
    configureFlags = [ "--with-ssl=${lib.getOutput "out" openssl}" ]
    ++ [ "--with-ssl-incl=${lib.getDev openssl}" ] ;
  };
  beamPkg = pkgs.beam.packagesWith erlang24;

  elixir12 = beamPkg.elixir.override {
   version = "1.12.3";
    # nix-prefetch-url --unpack https://github.com/elixir-lang/elixir/archive/refs/tags/v1.12.3.tar.gz
    sha256 = "07fisdx755cgyghwy95gvdds38sh138z56biariml18jjw5mk3r6";
  };
  elixir_ls7 = (
    beamPkg.elixir_ls.override {
      elixir = elixir12;
      mixRelease = beamPkg.mixRelease.override { elixir = elixir12; };
    }
  );
in pkgs.mkShell {
  ERL_AFLAGS = "-kernel shell_history enabled";

  buildInputs = [
    erlang24
    elixir12
    elixir_ls7
    git-crypt
  ];
}
