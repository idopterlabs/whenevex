defmodule Whenevex.RecurringTest do
  use ExUnit.Case, async: true
  doctest Whenevex

  import ExUnit.CaptureLog
  require Logger

  defmodule MyWorker do
    use Whenevex.Recurring

    def hello do
      Logger.info("hello!")
    end

    def hello_again do
      Logger.info("hello again!")
    end
  end

  describe "start recurring job" do
    test "start_link/1" do
      assert _pid = start_supervised!(MyWorker)
    end

    test "when the timer calls hello" do
      log =
        capture_log(fn ->
          # hello function will be call each 10ms
          start_supervised!({
            MyWorker, [
              {:hello, 10},
              {:hello_again, 5}
            ]
          })

          Process.sleep(120)
        end)

      assert log =~ "hello!"
      assert log =~ "hello again!"
    end
  end
end
