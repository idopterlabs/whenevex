defmodule Whenevex.Timer do
  require Logger
  use GenServer

  def start_link([function, interval]) do
    GenServer.start_link(__MODULE__, [function, interval])
  end

  def init([function, interval]) when is_function(function) and is_integer(interval) do
    get_default_interval()
    |> schedule()

    {:ok, {function, interval}}
  end

  def handle_call(:recall, _from, {function, interval} = state) do
    Logger.debug(fn -> "Running timer #{inspect(self())}" end)

    function.()
    schedule(interval)

    {:noreply, state}
  end

  defp schedule(interval) do
    Process.send_after(self(), {:"$gen_call", {self(), make_ref()}, :recall}, interval)
  end

  defp get_default_interval,
    do: System.get_env("RECURRING_DEFAULT_INTERVAL") |> get_default_interval()

  defp get_default_interval(nil), do: 99
  defp get_default_interval(interval) when is_binary(interval), do: String.to_integer(interval)
  defp get_default_interval(interval), do: interval
end
