defmodule Whenevex.Recurring do
  defmacro __using__(_opts) do
    quote do
      require Logger
      use GenServer

      @second 1_000
      @minute 60_000
      @hour 3_600_000

      def start_link(jobs) do
        GenServer.start_link(__MODULE__, [jobs], name: __MODULE__)
      end

      def init([jobs]) do
        Enum.each(jobs, fn {message, interval} ->
          timer = set_interval(interval)
          {:ok, _pid} = start_timer(message, timer)
        end)

        {:ok, :no_state}
      end

      def handle_call(message, _from, state) do
        apply(__MODULE__, message, [])

        {:reply, true, state}
      end

      defp start_timer(message, interval, timeout \\ 300_000) do
        pid = self()

        Whenevex.Timer.start_link([fn -> GenServer.call(pid, message, timeout) end, interval])
      end

      defp set_interval(interval) do
        case interval do
          :every_second -> @second
          :every_minute -> @minute
          :hourly -> @hour
          :daily -> 24 * @hour
          :weekly -> 7 * 24 * @hour
          _ -> interval
        end
      end
    end
  end
end
