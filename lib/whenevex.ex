defmodule Whenevex do
  @moduledoc """
  Documentation for `Whenevex`.
  """

  @doc """
  Hello world.

  ## Examples

      iex> Whenevex.hello()
      :world

  """
  def hello do
    :world
  end
end
